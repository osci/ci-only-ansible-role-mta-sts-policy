# Ansible role to manage MTA-STS Policy

## Introduction

[MTA-STS (RFC8461)](https://tools.ietf.org/html/rfc8461) is a way to advertize that the mail servers in charge of your domain expect to be contacted using a secure connection.

This role generates the MTA-STS configuration and deploys it in a proper vhost and updates the DNS record advertizing its existence.

To update the DNS without meddling with your zone this role creates a separate partial zone file which can easily be included from your main zone file.

You need to run the role on the webserver host. If your DNS server if on another machine, use the `dns_host` parameter.

Example playbook:

```
- hosts: www.example.com
  vars:
    domain: example.com
  tasks:
    - name: "Install MTA-STS"
      include_role:
        name: mta_sts_policy
      vars:
        document_root: "/var/www/{{ inventory_hostname }}"
        mx_list:
          - mx1.example.com
          - mx2.example.com
        dns_file: "/etc/bind/masters/{{ domain }}.mta-sts"
```

Example DNS configuration:

```
; MTA-STS (RFC 8461)
mta-sts                 IN CNAME        www
$INCLUDE /etc/bind/masters/example.com.mta-sts
```

(`touch /etc/bind/masters/example.com.mta-sts` before reloading the DNS configuration the first time)

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role bind9

```
