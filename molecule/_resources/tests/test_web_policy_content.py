import os

import testinfra.utils.ansible_runner

testinfra_ansible_runner = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
)
testinfra_hosts = testinfra_ansible_runner.get_hosts("ansible-test-dns")

web_facts = testinfra_ansible_runner.run_module("ansible-test-web", "setup", None)[
    "ansible_facts"
]
web_ip = web_facts["ansible_default_ipv4"]["address"]


def test_policy_content(host):
    result_file = "/tmp/test_web_policy_content"
    host.run_expect(
        [0],
        f"curl --resolve '*:80:{web_ip}' -o {result_file} http://mta-sts.example.org/.well-known/mta-sts.txt",
    )
    assert host.file(result_file).contains(
        "version: STSv1\nmode: enforce\nmx: mx1.example.org\nmx: mx2.example.org\nmax_age: 604800"
    )
