import os

import testinfra.utils.ansible_runner

testinfra_ansible_runner = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
)
testinfra_hosts = testinfra_ansible_runner.get_hosts("ansible-test-web")

dns_facts = testinfra_ansible_runner.run_module("ansible-test-dns", "setup", None)[
    "ansible_facts"
]
dns_ip = dns_facts["ansible_default_ipv4"]["address"]


def test_dns_entry(host):
    host.run_expect(
        [0], f"dig -4 ansible-test-dns _mta-sts.example.org.i TXT @{dns_ip}"
    )
